#!/bin/bash
#
# Axel Roest, 20200613
#
# Meant to be run from a --root-- cronjob, checking the wdt ups battery status every 5 minutes or so
# if running from battery, i.e. Voltage input = 0, and battery voltage < 3000 then
# shutdown (and perhaps send a last email or something)
#
# dependencies:
# - https://github.com/SequentMicrosystems/wdt-rpi, to interrogate the UPS board
# - ssmtp, part of mailutils, install with `apt-get install ssmtp mailutils`
#
# set the timezone on your Raspberry-Pi correctly if you live outside of the UK:
# `ln -s /usr/share/zoneinfo/Europe/Amsterdam /etc/localtime`
#


EMAIL=your_email_here@example.com

# location where you installed the wdt utility from SequentMicrosystems 
WDT=/usr/local/bin/wdt

# location where ssmtp is installed
SSMTP=/usr/sbin/ssmtp

VIN=`$WDT -g v`
VBAT=`$WDT -g vb`
VTHRESHOLD=3000

# For testing
# VIN=0
# VBAT=0

TODAY=`/bin/date +"%Y%m%d"`
LOG="/var/log/ups_$TODAY.log"
touch $LOG

HOSTNAME=`/bin/hostname`
NOW=`/bin/date +"%Y%m%dT%T"`

if [ "$VBAT" = "" ] ; then
	echo "$NOW UPS safe_shutdown, an error occurred reading the UPS." >> $LOG
	exit 1
fi

if [ $VIN -lt 3000 ] ; then
	
	if [ $VBAT -lt $VTHRESHOLD ] ; then
		echo "$NOW UPS Power issue, battery depleted: ($VBAT mV), Shutdown!" >> $LOG
#		echo "$NOW UPS Power issue, battery depleted: ($VBAT mV)"
		echo -e "Subject: $HOSTNAME Shutdown!\n\nBattery=${VBAT} mV\n\n${HOSTNAME} shutdown on $NOW" | $SSMTP $EMAIL
		$WDT -off 7200
		/sbin/shutdown -h now
	else 
		echo "$NOW UPS Power issue, running from battery: ($VBAT mV)" >> $LOG
#		echo "$NOW UPS Power issue, running from battery: ($VBAT mV)"
		echo -e "Subject: $HOSTNAME UPS Power issue\n\n${HOSTNAME} UPS Power issue on $NOW\nBattery=${VBAT} mV\n" | $SSMTP $EMAIL
	fi
fi
# echo "Power okay!"

exit 0
