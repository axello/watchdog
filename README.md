# Some scripts for the Sequent Microsystems Super watchdog+UPS

## Uninterruptable Power Supply (UPS)
I was pleasantly surprised when I discovered a [Kickstarter project](https://www.kickstarter.com/projects/279405789/hardware-watchdog-hat-and-power-manager-for-raspberry-pi?ref=user_menu) for a low-cost watchdog and UPS specifically for the Raspberry-Pi, including drivers. It is open source and was made by a manufacturer with a proven track record, [Sequent Microsystems](https://sequentmicrosystems.com).
![the Super Watchdog and UPS](images/swd-10-500x500.jpg)
## Tools
### safeshutdown.sh

Meant to be run from a --root-- cronjob, checking the wdt ups battery status every 5 minutes or so.
When running from the battery, i.e. with voltage input = 0, and the battery voltage gets below 3000 mV  then the script shuts down the computer (and perhaps send a last email or something)

### wdt-all
I simply wanted a script which outputs all the relevant details from the UPS, like battery and input voltage. It produces the following output:

```
Overview of watchdog settings
period              : 270  seconds
watchdog resets     : 478
Voltage input (mV)  : 5090
Voltage battery (mV): 3504
Voltage Pi (mV)     : 5056
Temperature (°C)    : 30
Battery Charging

```

### dependencies
* https://github.com/SequentMicrosystems/wdt-rpi, to interrogate the UPS board
* ssmtp, part of mailutils, install with `apt-get install ssmtp mailutils`


## Installation details

### raspi-config
The watchdog+ups communicates with the Pi over the I2C bus. You have to enable this with the `raspi-config` program as explained in the manual.

### library
The base software for the watchdog/ups is available on [GitHub](https://github.com/SequentMicrosystems/wdt-rpi).
It has a command line tool `wdt` and a python library.
My experience with python however is a mix of ***amazing!*** and cursing at apt-get + pip + easy-install and python versions. 

Instead I opted for the over forty year old stable shell script language to test the ups.

### watchdog
So, what *is* a watchdog, you might ask? Watchdogs detect system lock-ups and try to reboot the system. Sometimes this is all that's needed to keep a system running. Say you have an application which overloads the Pi *sometimes* but not enough to warrant a complete rewrite or exchange of the application. Or a server process goes haywire and locks the system. 
A watchdog reboots the system unscrupulously when it is not fed with an 'I'm alive' message every minute or so. It is the poor man's alternative of a fully-redundant server-park with automatic fail-over between servers.

Your application would 'ping' the watchdog periodically, say every 20 seconds, and if the watchdog did not get at least one ping within a minute's time, it will reset the host computer.

The watchdog needs to be set to watchdog modus using the `wdt -r` command. When this is set, you need to ping the watchdog within the interval time, otherwise your server is reset! You ping it using the reload command `wdt -r`
For my tests I used the following simple cron job. When I stopped the cronjob, the pi was indeed reset.

**crontab -l :**

`*/2 * * * * /usr/local/bin/wdt -r`

### caveats
Because there is now a watchdog available, a simple shutdown or reboot is now tricky! When you shut down the Pi, the watchdog does not get the pings it expects and will restart your Pi after the timeout time.

For that you can temporarily disable the watchdog like this:
`wdt -off 7200`
which disables the watchdog for 7200 seconds, or two hours.



